package sim.tsa;

import java.util.ArrayList;
import cern.jet.random.Uniform;

/* TravellingSalesAnt. Practical 3: Implement an agent capable of
 * finding a path through the space (step()) and updating the
 * pheromone levels proportionally to the quality of the solution
 * (step2()), according to an ant colony optimisation strategy.
 *
 */

public class TravellingSalesAnt {

  private TSAWorld space;
  private TSAModel model;
  private double length;
  private ArrayList visited;
  private ArrayList unvisited;

  public TravellingSalesAnt(TSAWorld ss, TSAModel model) {
    space = ss;
    this.model = model;
  }

  public ArrayList getPath() { return visited; }
  public double getLength() { return length; }

  public void step(TSACity start) {
    // some placeholder code. Add your code implementing a tour here 
    length = 0;
    visited = (ArrayList) space.getCitiesList().clone();
  }

  public void step2() {
    // add your code for pheromone distribution here
  }
}
